import { createContext } from "react";

const whySubscribeData =
  require("../SampleData/Data/WhySubscribe.json").whySubscribe;
const whyUsData = require("../SampleData/Data/WhyUs.json").whyUs;
const featuresData = require("../SampleData/Data/Features.json").features;
const faqData = require("../SampleData/Data/Faq.json").faq;
export const GlobalContext = createContext();

export default function ContextProvider(props) {
  return (
    <GlobalContext.Provider
      value={{ whySubscribeData, whyUsData, featuresData, faqData }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
}
