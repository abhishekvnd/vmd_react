import React, { useContext } from "react";
import { GlobalContext } from "../ContextProvider/ContextProvider";
import "./Features.css";

export default function Features() {
  const Features = useContext(GlobalContext).featuresData;

  return (
    <section className="features-base">
      <h2 className="feature-title">Many more Exciting Features</h2>

      <div className="features-options-container">
        {Features.map((feature, index) => (
          <div className="features-option" key={index}>
            <img src={feature.image} alt="" />
            <p>{feature.title}</p>
          </div>
        ))}
      </div>
    </section>
  );
}
