import React from "react";
import MaxWidthWrapper from "../general/MaxWidthWrapper";
import { PUBLIC_IMAGE_PATH } from "../utils/Constants";
import "./Footer.css";

const SOCIAL_LINKS = [
  {
    image: PUBLIC_IMAGE_PATH + "social-instagram.svg",
    alt: "Instagram",
    link: "http://www.facebook.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "social-twitter.svg",
    alt: "Twitter",
    link: "http://www.facebook.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "social-facebook.svg",
    alt: "Facebook",
    link: "http://www.facebook.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "social-whatsapp.svg",
    alt: "Whatsapp",
    link: "http://www.facebook.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "social-youtube.svg",
    alt: "Youtube",
    link: "http://www.facebook.com",
  },
];

const PAYMENT_LINKS = [
  {
    image: PUBLIC_IMAGE_PATH + "payment-mastercard.png",
    alt: "MasterCard",
    link: "http://www.google.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "payment-visa.png",
    alt: "Visa",
    link: "http://www.google.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "payment-americanexpress.png",
    alt: "AmericanExpress",
    link: "http://www.google.com",
  },

  {
    image: PUBLIC_IMAGE_PATH + "payment-paypal.png",
    alt: "PayPal",
    link: "http://www.google.com",
  },
  {
    image: PUBLIC_IMAGE_PATH + "payment-paytm.png",
    alt: "Paytm",
    link: "http://www.google.com",
  },
];

export default function Footer() {
  return (
    <footer className={"footer-base"}>
      <MaxWidthWrapper>
        <div className="footer-links-container">
          <ul className="footer-column">
            <li className="footer-logo">
              <img src={PUBLIC_IMAGE_PATH + "logo.png"} alt="Telephone" />
            </li>
            <li className="footer-contact-option">
              <span className="footer-option-icon">
                <img
                  src={PUBLIC_IMAGE_PATH + "footer-telephone.png"}
                  width={19}
                  height={19}
                  alt="Telephone"
                />
              </span>
              080 49577577
            </li>
            <li className="footer-contact-option">
              <span className="footer-option-icon">
                <img
                  src={PUBLIC_IMAGE_PATH + "footer-phone.svg"}
                  width={19}
                  height={19}
                  alt="Phone"
                />
              </span>
              +91 81056 76665
            </li>
            <li className="footer-contact-option">
              <span className="footer-option-icon">
                <img
                  src={PUBLIC_IMAGE_PATH + "footer-email.png"}
                  width={19}
                  height={19}
                  alt="Email"
                />
              </span>
              reachus@teamonk.com
            </li>
          </ul>

          <ul className="footer-column">
            <h3 className="footer-column-head">SHOP</h3>
            <li>Green Tea</li>
            <li>Black Tea</li>
            <li>White Tea</li>
            <li>Oolong Tea</li>
            <li>Herbal Infusions</li>
            <li>Gifts & Accessories</li>
            <li>Subscribe & Save</li>
          </ul>

          <ul className="footer-column">
            <h3 className="footer-column-head">ABOUT</h3>
            <li>Why Teamonk</li>
            <li>Teamonk Story</li>
            <li>Our Team</li>
            <li>Media</li>
            <li>Events</li>
            <li>Careers</li>
            <li>Track Order</li>
          </ul>

          <ul className="footer-column">
            <h3 className="footer-column-head">ABOUT</h3>
            <li>Why Teamonk</li>
            <li>Teamonk Story</li>
            <li>Our Team</li>
            <li>Media</li>
            <li>Events</li>
            <li>Careers</li>
            <li>Track Order</li>
          </ul>

          <ul className="footer-column">
            <h3 className="footer-column-head">BUSINESS</h3>
            <li>Corporate Gifting</li>
            <li>Associations</li>
            <li>Business Orders</li>
            <li>Coupon Partners</li>
            <li>Wishlist</li>
          </ul>

          <ul className="footer-column">
            <h3 className="footer-column-head">RESOURCES</h3>
            <li>Blog</li>
            <li>Tea Matters</li>
            <li>Tea Recipes</li>
            <li>Food Pairing</li>
          </ul>

          <ul className="footer-column">
            <h3 className="footer-column-head">INFORMATION</h3>
            <li>Terms of Use</li>
            <li>Privacy Policy</li>
            <li>Return Policy</li>
            <li>FAQs</li>
            <li>Contact Us</li>
            <li>Track Order</li>
          </ul>
        </div>

        <div className="footer-payments-container">
          <ul>
            {PAYMENT_LINKS.map((i) => (
              <li
                className="footer-payment-icon-container"
                onClick={() => window.open(i.link, "_blank").focus()}
              >
                <img
                  className="footer-payment-icon"
                  src={i.image}
                  alt={i.alt}
                />
              </li>
            ))}
          </ul>
        </div>

        <div className="footer-bottom-container">
          <div className="footer-trademark">
            © 2021, Teamonk , All rights reserved.
          </div>
          <ul className="footer-social-links">
            {SOCIAL_LINKS.map((i) => (
              <li
                className="footer-social-icon-container"
                onClick={() => window.open(i.link, "_blank").focus()}
              >
                <img
                  className="footer-social-icon"
                  src={i.image}
                  alt={i.alt}
                  height={20}
                />
              </li>
            ))}
          </ul>
        </div>
      </MaxWidthWrapper>
    </footer>
  );
}
