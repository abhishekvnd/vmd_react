import React from "react";
import "./Home.css";
import SubscriptionCard from "../SubscriptionCard/SubscriptionCard";
import Button from "../general/Button";
import { PUBLIC_IMAGE_PATH } from "../utils/Constants";

export default function Home() {
  function renderSubscriptionCards(title, subscriptionCards = []) {
    return (
      <>
        <h3 className="home-subscription-head">{title}</h3>
        <div className="home-subscription-cards-container">
          {subscriptionCards.map((card, index) => (
            <SubscriptionCard
              key={index}
              title={card.title}
              subTitle={card.subTitle}
              description={card.description}
              label="Select"
            />
          ))}
        </div>
      </>
    );
  }
  return (
    <section className="home-base">
      <Button
        label="Start Here"
        width={224}
        height={54}
        backgroundImage={PUBLIC_IMAGE_PATH + "button-bg.png"}
        textStyle={{ fontSize: 20, fontWeight: 600 }}
      />

      {renderSubscriptionCards("Choose your subscription", [
        {
          title: "Discovery Plan",
          subTitle: "Free Delivery",
          description: "Choose from a Range of Natural Teas.",
          label: "Select",
        },

        {
          title: "Wellness Plan",
          subTitle: "Free Delivery",
          description: "Curated by Our Tea Connoisseurs.",
          label: "Select",
        },

        {
          title: "Freedom Plan",
          subTitle: "Free Delivery",
          description: "Choose the Perfect Cup with a few extra steps.",
          label: "Select",
        },
      ])}

      {renderSubscriptionCards("Tenure of subscription", [
        {
          title: "Discovery Plan",
          subTitle: "Free Delivery",
          description: "Choose from a Range of Natural Teas.",
          label: "Select",
        },
      ])}
    </section>
  );
}
