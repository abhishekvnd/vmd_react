export default function MaxWidthWrapper(props) {
  return (
    <div
      className="MaxWidthWrapper"
      style={{
        maxWidth: props.maxWidth,
      }}
    >
      {props.children}
    </div>
  );
}

MaxWidthWrapper.defaultProps = {
  maxWidth: "1660px",
};
