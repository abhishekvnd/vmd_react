import React from "react";
import { PUBLIC_IMAGE_PATH } from "../utils/Constants";
import "./Thanking.css";

export default function Thanking() {
  return (
    <section className="thanking-base">
      <h3 className="thanking-title">Thank you for choosing TEAMONK!</h3>
      <p className="thanking-description">
        Your tea subscription is almost complete. When your first box arrives,
        you will be so pleased that you have joined a unique tea club. You'll
        also be tasting ethically sourced specialty tea that is often organic
        and has been purchased directly from the tea growers. Your support
        allows the farmer to invest in a sustainable crop and improve the
        quality of life for his children and community. In addition, our tea
        concierges regularly visit the estate or plantation where your tea is
        grown to ensure that everyone working on the farm, from the farmer to
        the picker, complies with international labor laws, social and
        environmental standards. We take you on a journey through the world of
        tea through our tea club. labor laws, social and environmental
        standards. We take you on a journey through the world of tea through our
        tea club.
        <br />
        <br />
        You will be pleasantly surprised by the distinct flavors and aroma.
        Month after month, your tea club subscription will continue to amaze
        you.
      </p>

      <img
        src={PUBLIC_IMAGE_PATH + "thanking.png"}
        className="thanking-img"
        alt=""
      />
    </section>
  );
}
