import { useContext } from "react";
import { GlobalContext } from "../ContextProvider/ContextProvider";
import "./FAQ.css";

export default function FAQ() {
  const FAQs = useContext(GlobalContext).faqData;

  return (
    <section className="faq-base">
      <h2 className="faq-title">Frequently asked questions</h2>
      <ul className="faq-list">
        {FAQs.map((faq, index) => (
          <li key={index}>
            <p className="faq-ques">{faq.question}</p>
            <p className="faq-ans">{faq.answer}</p>
          </li>
        ))}
      </ul>
    </section>
  );
}
