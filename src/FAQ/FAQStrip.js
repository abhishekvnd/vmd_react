import React from "react";

export default function FAQStrip() {
  return (
    <section className="faqStrip-base">
      Got a Question? Take a Look at Our Faqs or Give Us a Call on
      +91-80-49577577
    </section>
  );
}
