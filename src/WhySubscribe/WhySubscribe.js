import { useContext } from "react";
import { GlobalContext } from "../ContextProvider/ContextProvider";
import { PUBLIC_IMAGE_PATH } from "../utils/Constants";
import "./WhySubscribe.css";

export default function WhySubscribe() {
  const WhySubscribe = useContext(GlobalContext).whySubscribeData;

  return (
    <section className="whysubscribe-base">
      <h2>How does it work?</h2>

      <div className="whysubscribe-content-container">
        <ul>
          {WhySubscribe.map((i, index) => (
            <li key={index}>
              <p className="whysubscribe-list-title">{i.title}</p>
              <p className="whysubscribe-list-desc">{i.description}</p>
            </li>
          ))}
        </ul>

        <img src={PUBLIC_IMAGE_PATH + "about-subscription.png"} alt="" />
      </div>
    </section>
  );
}
