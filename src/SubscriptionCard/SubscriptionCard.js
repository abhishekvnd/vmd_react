import React, { useState } from "react";
import Button from "../general/Button";
import "./SubscriptionCard.css";

export default function SubscriptionCard(props) {
  const [hovered, setHovered] = useState(false);

  return (
    <div
      className="subscriptionCardBase"
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <p className="subscriptionCard-title">{props.title}</p>
      <p className="subscriptionCard-subTitle">{props.subTitle}</p>
      <p className="subscriptionCard-description">{props.description}</p>
      {props.selected && (
        <ul className="subscriptionCard-selected-base">
          <li>3</li>
          <li>6</li>
          <li>9</li>
          <li>12</li>
        </ul>
      )}

      <Button
        label={props.label}
        backgroundColor="var(--primary-color)"
        hoverParent={{
          hovered: hovered,
          backgroundColor: "#fff",
          color: "#000",
        }}
      />
    </div>
  );
}
