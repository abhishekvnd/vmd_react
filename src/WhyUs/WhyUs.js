import React, { useContext } from "react";
import { GlobalContext } from "../ContextProvider/ContextProvider";
import { PUBLIC_IMAGE_PATH } from "../utils/Constants";
import "./WhyUs.css";

export default function WhyUs() {
  const WhyUs = useContext(GlobalContext).whyUsData;
  return (
    <section className="whyus-base">
      <img src={PUBLIC_IMAGE_PATH + "whyus.png"} alt="" className="whyus-img" />
      <div className="whyus-content-container">
        <h2 className="whyus-title">Why Choose Teamonk?</h2>

        <ul>
          {WhyUs.map((i, index) => (
            <li key={index} className="whyus-list">
              {i}
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
}
