import ContextProvider from "./ContextProvider/ContextProvider";
import FAQ from "./FAQ/FAQ";
import FAQStrip from "./FAQ/FAQStrip";
import Features from "./Features/Features";
import Footer from "./Footer/Footer";
import Button from "./general/Button";
import Home from "./Home/Home";
import Thanking from "./Thanking/Thanking";
import { PUBLIC_IMAGE_PATH } from "./utils/Constants";
import WhySubscribe from "./WhySubscribe/WhySubscribe";
import WhyUs from "./WhyUs/WhyUs";

function App() {
  return (
    <ContextProvider>
      <Home />
      <Thanking />
      <WhySubscribe />
      <WhyUs />
      <Features />
      <FAQStrip />
      <FAQ />
      <div
        style={{ display: "flex", justifyContent: "center", marginBottom: 45 }}
      >
        <Button
          label="Get Started"
          width={224}
          height={54}
          backgroundImage={PUBLIC_IMAGE_PATH + "button-bg.png"}
          textStyle={{ fontSize: 20, fontWeight: 600 }}
        />
      </div>
      <Footer />
    </ContextProvider>
  );
}

export default App;
