# Teamonk Tea Subscription


Web App built using react to subscribe to Teamonk Tea.


## Steps to run
- Install packages
   -  ``yarn ``
- Now finally let's start running the project
    - `` yarn start ``
    - Runs the app in the development mode.\
      Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
